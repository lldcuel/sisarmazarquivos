package constantesEOutros;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestesComArqsEDirs {
    
    public static void main(String[] args) {
        Path currPath = Paths.get("");
        Path currPath2 = currPath.toAbsolutePath();
        System.out.println("a");
        System.out.println(currPath2);
        System.out.println(currPath2.getParent());
        System.out.println(currPath2.getRoot());
        
        Path path2 = Paths.get("C://Users//lucas//Dropbox");
        System.out.println(path2.getParent());
        System.out.println(path2.getParent().getParent());
        System.out.println(path2.getRoot());
        
        if(currPath2.getParent().getParent().equals(path2))
            System.out.println("yes");
        
        System.out.println("Path completo: " + currPath2);
        for (int i = 0; i < 5; i++) {
            System.out.println("Subpasta " + i + ": " + currPath2.getName(i));
        }
        
        Iterator<Path> it = currPath2.iterator();
        while(it.hasNext()){
            Path el = it.next();
            System.out.println("element: " + el);
        }
        
        System.out.println("system separator: " + File.separator);
        File f1 = new File(currPath2.toFile(), "filho.txt");
        File f2 = new File(currPath2.toFile(), "filho2.txt");
        try {
            f1.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(TestesComArqsEDirs.class.getName()).log(Level.SEVERE, null, ex);
        }
        f1.renameTo(f2);
        System.out.println(currPath2.toFile());
        System.out.println("f1: " + f1);
        
        File folder1 = new File(currPath2.toFile(), "\\folderTest");
        File folder2 = new File(currPath2.toFile(), "\\folderAlterado");
        folder1.renameTo(folder2);
        
        File folder = currPath2.toFile();
        File[] listOfFiles = folder.listFiles();

        for (File currFile : listOfFiles) {
            if (currFile.isFile()) {
                System.out.println("File " + currFile.getName());
            } else if (currFile.isDirectory()) {
                System.out.println("Directory " + currFile.getName());
            }
        }
        
    }
    
}
