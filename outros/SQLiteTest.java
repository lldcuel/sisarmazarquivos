package constantesEOutros;

import dao.DAO;

public class SQLiteTest {

    public static void main(String[] args) {

        DAO db = new DAO();

        db.exec("CREATE TABLE t1 (t1key INTEGER PRIMARY KEY,data TEXT,num double,timeEnter DATE);");
        db.exec("INSERT INTO t1 VALUES(1, 'This is sample data', 3, NULL);");
        db.exec("INSERT INTO t1 VALUES(2, 'More sample data', 6, NULL);");
        db.exec("INSERT INTO t1 VALUES(3, 'And a little more', 9, NULL);");

        Object[] res;
        db.prepareQuery("select * from t1");

        while ((res = db.fetch()) != null) {
            System.out.println("Results: " + res[0] + " : " + res[1] + " : " + res[2] + " : " + res[3]);
        }

        db.close();

    }
}
