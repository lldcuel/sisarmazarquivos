package model;

import java.io.Serializable;

public class Host implements Comparable, Serializable {
    
    private String ip;
    
    private int id;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int compareTo(Object o) {
        Host x = (Host) o;
        if(this.id < x.getId()){
            return -1;
        }else if(this.getId() > x.getId()){
            return 1;
        }else {
            return 0;
        }
    }
    
    @Override
    public boolean equals(Object o){
        Host h = (Host) o;
        if(this.getId() == h.getId() || this.getIp().equals(h.getIp())){
            return true;
        } else {
            return false;
        }
    }
    
}
