/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author lshigaki
 */
public class Arquivo {
    
    private int id;
    private int id_usuario;
    private int id_pasta;
    private String nome;
    private int tamanho;
    private String backup_ip;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_pasta() {
        return id_pasta;
    }

    public void setId_pasta(int id_pasta) {
        this.id_pasta = id_pasta;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public String getBackup_ip() {
        return backup_ip;
    }

    public void setBackup_ip(String backup_ip) {
        this.backup_ip = backup_ip;
    }

}
