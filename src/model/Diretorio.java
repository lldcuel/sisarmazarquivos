package model;

public class Diretorio implements Comparable {
    
    private String dir;
    
    private int qtdadeSep;

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public int getQtdadeSep() {
        return qtdadeSep;
    }

    public void setQtdadeSep(int qtdadeSep) {
        this.qtdadeSep = qtdadeSep;
    }

    @Override
    public int compareTo(Object o) {
        Diretorio x = (Diretorio) o;
        if(this.getQtdadeSep() < x.getQtdadeSep()){
            return -1;
        }else if(this.getQtdadeSep() > x.getQtdadeSep()){
            return 1;
        }else {
            return 0;
        }
    }
    
}
