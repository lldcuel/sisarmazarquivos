/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author lshigaki
 */
public class Usuario implements Serializable{
    
    private int id;
    private String login;
    private String senha;
    private String ip;
    private String ipBackup;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getIpBackup() {
        return ipBackup;
    }

    public void setIpBackup(String ipBackup) {
        this.ipBackup = ipBackup;
    }
   
}
