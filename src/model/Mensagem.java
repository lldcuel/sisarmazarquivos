package model;

public class Mensagem {
    
    private int qtdade;
    private String nomeDaFuncao;
    private String ip;
    private String path;
    private String login;
    private String senha;
    private String path2;

    public String getPath2() {
        return path2;
    }

    public void setPath2(String path2) {
        this.path2 = path2;
    }
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public int getQtdade() {
        return qtdade;
    }

    public void setQtdade(int qtdade) {
        this.qtdade = qtdade;
    }

    public String getNomeDaFuncao() {
        return nomeDaFuncao;
    }

    public void setNomeDaFuncao(String nomeDaFuncao) {
        this.nomeDaFuncao = nomeDaFuncao;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
}
