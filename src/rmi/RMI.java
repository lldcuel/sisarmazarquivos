package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import model.Usuario;

public interface RMI extends Remote {
    
    public String helloRMI(String text) throws RemoteException;
    
    public byte[] downloadFile(String username, String pathname, String password) throws RemoteException;
    
    public boolean uploadFile(String username, String pathname, byte[] file, String password, boolean backup) throws RemoteException;
    
    public List<String> listFiles(String username, String password) throws RemoteException;
    
    public List<String> listDirectories(String username, String password) throws RemoteException;
    
    public boolean copyFile(String filenameWithPath, String destinationDir, String username, String password, boolean backup) throws RemoteException;
    
    public boolean moveFile(String filenameWithPath, String destinationDir, String username, String password, boolean backup) throws RemoteException;
    
    public boolean deleteFile(String filenameWithPath, String username, String password, boolean backup) throws RemoteException;
    
    public boolean createDirectory(String dadDir, String directoryName, String username, String password, boolean backup) throws RemoteException;
    
    public boolean deleteDirectory(String dadDir, String directoryName, String username, String password, boolean backup) throws RemoteException;
    
    public List<String> createUser(String username, String password) throws RemoteException;
    
    public Usuario authenticateUserAndReturnInfos(String username, String password) throws RemoteException;
    
    public void updateUser(Usuario user) throws RemoteException;
    
}
