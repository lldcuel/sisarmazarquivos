package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import model.Host;
import model.Mensagem;
import model.Usuario;

public interface RMICommc extends Remote {
    
    public boolean areYouAlive(String text) throws RemoteException;
        
    public List<Host> joinSystem(String ipIngressante) throws RemoteException;
        
    public void deleteServerFromMeuGrupo(String ipToDelete, int qtdade) throws RemoteException;
    
    public void enviaIPs(List<Host> listaDeHosts, int qtdade) throws RemoteException;
        
    public void enviaBackup(Mensagem m) throws RemoteException;
    
    public long sizeOfPath () throws RemoteException;
    
    public List<String> allActiveServers() throws RemoteException;
    
    public List<String> allFilesOfTheServer() throws RemoteException;
    
    public void enviaNovoUsuario(Usuario usuario, int qtdade) throws RemoteException;
    
    public void inserirUsuariosPqEuSouNovo(List<Usuario> usersList) throws RemoteException;
    
}
