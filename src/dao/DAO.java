package dao;

import com.almworks.sqlite4java.*;
import global.Const;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import model.Host;
import model.Mensagem;
import model.Usuario;

public class DAO {

    SQLiteConnection db;
    SQLiteStatement st;

    public DAO() {
        
    }
    
    public void openConnection(){
        try {
            java.util.logging.Logger.getLogger("com.almworks.sqlite4java").setLevel(Const.LOG_LEVEL);
            db = new SQLiteConnection(new File(Const.BD_FILE_NAME));
            db.open(true);
        } catch (SQLiteException ex) {
            System.out.println("Instantiation SQLiteException: " + ex.getMessage());
        }
    }

    public boolean exec(String str) {
        try {
            st = db.prepare(str);
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean ehPraParar(){
        try {
            st = db.prepare("SELECT * FROM Break;");
            
            int r = 0;
            try {
                if(st.step()){
                    r = st.columnInt(0);
                }
            } finally {
                if(st != null){if(!st.isDisposed()){st.dispose();}}
            }
            
            if(r==1){
                return true;
            } else {
                return false;
            }
            
        } catch (SQLiteException ex) {
            //st.dispose();
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
        }
        
        return false;
    }
    
    public boolean setParar1(){
        try {
            st = db.prepare("UPDATE Break SET parar = 1;");
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean setParar0(){
        try {
            st = db.prepare("UPDATE Break SET parar = 0;");
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean deletaParar(){
        try {
            st = db.prepare("DELETE FROM Break;");
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean inicializaBreak(){
        try {
            st = db.prepare("INSERT INTO Break(parar) VALUES(?);");
            
            st.bind(1, 0);
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean insertIntoUsuario(Usuario user){
        try {
            st = db.prepare("INSERT INTO usuario(ip, login, senha, ipBackup) VALUES(?, ?, ?, ?);");
            
            st.bind(1, user.getIp());
            st.bind(2, user.getLogin());
            st.bind(3, user.getSenha());
            st.bind(4, user.getIpBackup());
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean setNewIpForUser(String newIp, String user){
        try {
            st = db.prepare("UPDATE usuario SET ip = ? WHERE login = ?;");
            
            st.bind(1, newIp);
            st.bind(2, user);
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean setNewIpBackupForUser(String newIpBackup, String user){
        try {
            st = db.prepare("UPDATE usuario SET ipBackup = ? WHERE login = ?;");
            
            st.bind(1, newIpBackup);
            st.bind(2, user);
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean insertIntoMensagem(Mensagem m){
        try {
            st = db.prepare("INSERT INTO Mensagem(nomeDaFuncao, qtdade) VALUES(?, ?);");
            
            st.bind(1, m.getNomeDaFuncao());
            st.bind(2, m.getQtdade());
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }

    public boolean insertIntoMensagemWithIP(Mensagem m){
        try {
            st = db.prepare("INSERT INTO Mensagem(nomeDaFuncao, qtdade, ip) VALUES(?, ?, ?);");
            
            st.bind(1, m.getNomeDaFuncao());
            st.bind(2, m.getQtdade());
            st.bind(3, m.getIp());
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean insertIntoMensagemBackup(Mensagem m){
        try {
            st = db.prepare("INSERT INTO Mensagem(nomeDaFuncao, qtdade, ip, path, login, senha, path2) VALUES(?, ?, ?, ?, ?, ?, ?);");
            
            st.bind(1, m.getNomeDaFuncao());
            st.bind(2, m.getQtdade());
            st.bind(3, m.getIp());
            st.bind(4, m.getPath());
            st.bind(5, m.getLogin());
            st.bind(6, m.getSenha());
            st.bind(7, m.getPath2());
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public Usuario readUsuario(String username){
        try {
            st = db.prepare("SELECT * FROM usuario WHERE login = ?;");
            
            st.bind(1, username);
            
            Usuario usuario = new Usuario();
            try {
                while(st.step()){
                    usuario.setId(st.columnInt(0));       
                    usuario.setIp(st.columnString(1));
                    usuario.setLogin(st.columnString(2));
                    usuario.setSenha(st.columnString(3));
                    usuario.setIpBackup(st.columnString(4));
                }
            } finally {
                if(st != null){if(!st.isDisposed()){st.dispose();}}
            }
            
            return usuario;
        } catch (SQLiteException ex) {
            st.dispose();
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
        }
        
        return null;
    }
    
    public List<Usuario> readAllUsuarios(){
        try {
            st = db.prepare("SELECT * FROM usuario;");
            
            List<Usuario> lista = new ArrayList<>();
            Usuario usuario;
            try {
                while(st.step()){
                    usuario = new Usuario();
                    usuario.setId(st.columnInt(0));       
                    usuario.setIp(st.columnString(1));
                    usuario.setLogin(st.columnString(2));
                    usuario.setSenha(st.columnString(3));
                    usuario.setIpBackup(st.columnString(4));
                    lista.add(usuario);
                }
            } finally {
                if(st != null){if(!st.isDisposed()){st.dispose();}}
            }
            
            return lista;
        } catch (SQLiteException ex) {
            st.dispose();
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
        }
        
        return null;
    }
    
    public List<Host> readMeuGrupo(){
        try {
            st = db.prepare("SELECT * FROM MeuGrupo ORDER BY id;");
            
            List<Host> lista = new ArrayList<>();
            Host host;
            try {
                while(st.step()){
                    host = new Host();
                    host.setId(st.columnInt(0));
                    host.setIp(st.columnString(1));
                    lista.add(host);
                }
            } finally {
                if(st != null){if(!st.isDisposed()){st.dispose();}}
            }
            
            return lista;
        } catch (SQLiteException ex) {
            st.dispose();
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
        }
        
        return null;
    }
    
    public List<Usuario> readServidorUsuarios(String ip){
        try {
            st = db.prepare("SELECT * FROM Usuario WHERE ip = ? ORDER BY id;");
            st.bind(1, ip);
            
            List<Usuario> lista = new ArrayList<>();
            Usuario usuario;
            try {
                while(st.step()){
                    usuario = new Usuario();
                    usuario.setLogin(st.columnString(2));
                    lista.add(usuario);
                }
            } finally {
                st.dispose();
            }
            
            return lista;
        } catch (SQLiteException ex) {
            st.dispose();
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
        }
        
        return null;
    }
    
     public Mensagem readMensagens(){
        try {
            st = db.prepare("SELECT * FROM Mensagem;");
            
            Mensagem m = new Mensagem();
            try {
                if(st.step()){
                    m.setNomeDaFuncao(st.columnString(0));
                    m.setQtdade(st.columnInt(1));
                    m.setIp(st.columnString(2));
                    m.setPath(st.columnString(3));
                    m.setLogin(st.columnString(4));
                    m.setSenha(st.columnString(5));
                    m.setPath2(st.columnString(6));
                }
            } finally {
                if(st != null){if(!st.isDisposed()){st.dispose();}}
            }
            
            return m;
        } catch (SQLiteException ex) {
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
        }
        
        return null;
    }
     
    public boolean deleteFromMensagem(){
        try {
            st = db.prepare("DELETE FROM Mensagem;");
                        
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public int contaCoordenadores(){
        try {
            st = db.prepare("SELECT count(id) FROM coordenador;");
            
            int resultado = -1;
            try {
                if(st.step())
                    resultado = st.columnInt(0);
            } finally {
                if(st != null){if(!st.isDisposed()){st.dispose();}}
            }
            
            return resultado;
        } catch (SQLiteException ex) {
            st.dispose();
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return -1;
        }
    }
    
    public int contaMeuGrupo(){
        try {
            st = db.prepare("SELECT count(id) FROM MeuGrupo;");
            
            int resultado = -1;
            try {
                if(st.step())
                    resultado = st.columnInt(0);
            } finally {
                if(st != null){if(!st.isDisposed()){st.dispose();}}
            }
            
            return resultado;
        } catch (SQLiteException ex) {
            st.dispose();
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return -1;
        }
    }
    
    public int selectMaxMeuGrupo(){
        try {
            st = db.prepare("SELECT max(id) FROM MeuGrupo;");
            
            int resultado = -1;
            try {
                if(st.step())
                    resultado = st.columnInt(0);
            } finally {
                if(st != null){if(!st.isDisposed()){st.dispose();}}
            }
            
            return resultado;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return -1;
        }
    }
    
    public boolean insertIntoMeuGrupo(Host host){
        try {
            st = db.prepare("INSERT INTO MeuGrupo(ip, id) VALUES(?, ?);");
            
            st.bind(1, host.getIp());
            st.bind(2, host.getId());
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean insertIntoMeuGrupoSemId(Host host){
        try {
            st = db.prepare("INSERT INTO MeuGrupo(ip) VALUES(?);");
            
            st.bind(1, host.getIp());
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public Host retriveIdAfterInsert(){
        int resultado = -1;
        try {
            st = db.prepare("SELECT max(id) FROM MeuGrupo;");
            
            try {
                if(st.step())
                    resultado = st.columnInt(0);
            } finally {
                if(st != null){if(!st.isDisposed()){st.dispose();}}
            }            
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return null;
        }
        try {
            st = db.prepare("SELECT * FROM MeuGrupo WHERE id = ?;");
            
            st.bind(1, resultado);
            
            Host host = new Host();
            try {
                if(st.step()){
                    host.setId(st.columnInt(0));
                    host.setIp(st.columnString(1));
                }
            } finally {
                if(st != null){if(!st.isDisposed()){st.dispose();}}
            }
            return host;
        } catch (SQLiteException ex) {
            st.dispose();
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return null;
        }
    }
    
    public boolean deleteFromMeuGrupo(Host host){
        try {
            st = db.prepare("DELETE FROM MeuGrupo WHERE id = ?;");
            
            st.bind(1, host.getId());
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean deleteFromMeuGrupoByIP(String ipToDelete){
        try {
            st = db.prepare("DELETE FROM MeuGrupo WHERE ip = ?;");
            
            st.bind(1, ipToDelete);
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean deleteAllFromMeuGrupo(){
        try {
            st = db.prepare("DELETE FROM MeuGrupo;");
            
            st.stepThrough();
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            
            return true;
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            if(Const.PRINT_EXCEPTION){System.out.println("Query Execution SQLiteException: " + ex.getMessage());}
            return false;
        }
    }
    
    public boolean prepareQuery(String str) {
        try {
            st = db.prepare(str);
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            System.out.println("Prepare Query SQLiteException: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public Object[] fetch() {
        try {
            if (!st.step()) {
                if(st != null){if(!st.isDisposed()){st.dispose();}}
                return null;
            } else if (st.hasRow()) {
                int columns = st.columnCount();
                Stack stack = new Stack();
                for (int column = 0; column < columns; column++) {
                    stack.push(st.columnValue(column));
                }
                return stack.toArray();
            } else {
                if(st != null){if(!st.isDisposed()){st.dispose();}}
                return null;
            }
        } catch (SQLiteException ex) {
            if(st != null){if(!st.isDisposed()){st.dispose();}}
            System.out.println("Fetch SQLiteException: " + ex.getMessage());
        }
        if(st != null){if(!st.isDisposed()){st.dispose();}}
        return null;
    }

    public void close() {
        db.dispose();
    }
}
