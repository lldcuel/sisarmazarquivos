package serverrmi;

import global.Const;
import dao.DAO;
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Mensagem;

public class ThreadAux implements Runnable {
    
    private final int qtdade;
    
    public ThreadAux(int qtdade){
        this.qtdade = qtdade;
    }

    @Override
    public void run() {
        try {
            sleep(3 * Const.TIME_TO_SLEEP + 1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DAO dao = new DAO();
        dao.openConnection();
        
        Mensagem m = new Mensagem();
        System.out.println("criando a msg de enviarAllUsersToNewServer no joinSystemThreadAux");
        m.setNomeDaFuncao("enviarAllUsersToNewServer");
        m.setQtdade(this.qtdade);
        dao.insertIntoMensagem(m);

        dao.close();
    }
    
}
