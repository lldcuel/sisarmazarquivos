package serverrmi;

import clientrmi.RMIClientCommc;
import global.Const;
import java.io.File;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Usuario;
import rmi.RMI;
import dao.DAO;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.security.Policy;
import model.Host;
import model.Mensagem;
import rmi.RMICommc;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.rmi.NotBoundException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
import static java.lang.Thread.sleep;
import javax.swing.JOptionPane;

public class ServerRMI extends UnicastRemoteObject implements RMI, RMICommc {

    private static int myFutureId;
    private static List<String> listaaa;

    public ServerRMI() throws RemoteException {
        super();
        listaaa = new ArrayList<>();
    }

    @Override
    public long sizeOfPath() {
        Path path = (Path) FileSystems.getDefault().getPath("", "");
        path = path.toAbsolutePath();

        final AtomicLong size = new AtomicLong(0);

        try {
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult
                        visitFile(Path file, BasicFileAttributes attrs) {

                    size.addAndGet(attrs.size());
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult
                        visitFileFailed(Path file, IOException exc) {

                    System.out.println("skipped: " + file + " (" + exc + ")");
                    // Skip folders that can't be traversed
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult
                        postVisitDirectory(Path dir, IOException exc) {

                    if (exc != null) {
                        System.out.println("had trouble traversing: " + dir + " (" + exc + ")");
                    }
                    // Ignore errors traversing a folder
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            throw new AssertionError("walkFileTree will not throw IOException if the FileVisitor does not");
        }

        return size.get();
    }

    public static boolean authenticateUser(String username, String password) {
        DAO db = new DAO();
        db.openConnection();

        Usuario userData = db.readUsuario(username);

        db.close();

        if (userData != null) {
            if (userData.getSenha().equals(password)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    @Override
    public Usuario authenticateUserAndReturnInfos(String username, String password) {
        DAO db = new DAO();
        db.openConnection();

        Usuario userData = db.readUsuario(username);

        db.close();

        if (userData != null) {
            if (userData.getSenha().equals(password)) {
                return userData;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    
    @Override
    public void updateUser(Usuario user) throws RemoteException {
        DAO db = new DAO();
        db.openConnection();
        
        db.setNewIpForUser(user.getIp(), user.getLogin());
        db.setNewIpBackupForUser(user.getIpBackup(), user.getLogin());
        
        db.close();
    }

    @Override
    public String helloRMI(String text) throws RemoteException {
        text = "Hi! ;)";
        System.out.println("OBJ_SERVER: oi");
        return text;
    }

    @Override
    public byte[] downloadFile(String username, String pathname, String password) throws RemoteException {
        if (!ServerRMI.authenticateUser(username, password)) {
            return null;
        }

        Path path = (Path) FileSystems.getDefault().getPath("", "");
        path = path.toAbsolutePath();
        path = path.resolve(username);
        path = path.resolve(pathname);

        byte[] data = null;
        try {
            data = Files.readAllBytes(path);
        } catch (IOException ex) {
            Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }

    @Override
    public boolean uploadFile(String username, String pathname, byte[] file, String password, boolean backup) throws RemoteException {
        if (!ServerRMI.authenticateUser(username, password)) {
            return false;
        }

        Path path = (Path) FileSystems.getDefault().getPath("", "");
        path = path.toAbsolutePath();
        path = path.resolve(username);
        path = path.resolve(pathname);

        if (backup) {
            DAO db = new DAO();
            db.openConnection();

            /*Decidir IP do Backup e Atualizar no Banco
        List<Host> hList = db.readMeuGrupo();
        List<Usuario> uList = null;
        Path path3 = (Path) FileSystems.getDefault().getPath(pathname, "");
        
        for (Host h : hList){
            
            uList = db.readServidorUsuarios(h.getIp());
            for (Usuario u : uList) {
                
            }
        }
             */
            Usuario u = db.readUsuario(username);

            Mensagem m = new Mensagem();
            m.setNomeDaFuncao("uploadBackup");
            m.setIp(u.getIpBackup());
            m.setPath(pathname);
            m.setLogin(username);
            m.setSenha(password);

            db.insertIntoMensagemBackup(m);

            db.close();
        }

        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(path.toString());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        try {
            stream.write(file);
        } catch (IOException ex) {
            Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        try {
            stream.close();
        } catch (IOException ex) {
            Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public static void walkFile(String path) {

        File root = new File(path);
        File[] list = root.listFiles();

        if (list == null) {
            return;
        }

        for (File f : list) {
            if (f.isDirectory()) {
                walkFile(f.getAbsolutePath());
                //System.out.println( "Dir:" + f.getAbsoluteFile() );
                //listaaa.add("Dir:" + f.getAbsoluteFile() + "\n");
            } else {
                //System.out.println( "File:" + f.getAbsoluteFile() );
                listaaa.add("File:" + f.getAbsoluteFile() + "\n");
            }
        }
    }

    public static void walkDir(String path) {

        File root = new File(path);
        File[] list = root.listFiles();

        if (list == null) {
            return;
        }

        for (File f : list) {
            if (f.isDirectory()) {
                walkDir(f.getAbsolutePath());
                //System.out.println( "Dir:" + f.getAbsoluteFile() );
                if (f.isDirectory()) {
                    listaaa.add("Dir:" + f.getAbsoluteFile() + "\n");
                }
            }
        }
    }

    @Override
    public List<String> allFilesOfTheServer() {
        Path path = (Path) FileSystems.getDefault().getPath("", "");
        path = path.toAbsolutePath();

        String s = path.toString();
        String pi[];
        pi = s.split(Pattern.quote("\\"));
        String oqeq = pi[pi.length - 1];
        System.out.println("HA: " + oqeq);

        ServerRMI.walkFile(path.toString());
        List<String> list = listaaa;
        List<String> list2 = new ArrayList<>();
        for (String string : list) {
            String pieces[] = string.split(oqeq);
            //System.out.println("k" +  pieces.length + pieces[0] + pieces[1]);

            list2.add(pieces[pieces.length - 1]);
        }
        
        listaaa.clear();
        return list2;
    }

    @Override
    public List<String> listFiles(String username, String password) throws RemoteException {
        if (!ServerRMI.authenticateUser(username, password)) {
            return null;
        }

        Path path = (Path) FileSystems.getDefault().getPath("", "");
        path = path.toAbsolutePath();
        path = path.resolve(username);

        /*
        File folder = path.toFile();
        File[] listOfFiles = folder.listFiles();

        List<String> list = new ArrayList<>();
        for (File currFile : listOfFiles) {
            if (currFile.isFile()) {
                list.add(currFile.getName());
            }
        }
         */
        ServerRMI.walkFile(path.toString());
        List<String> list = listaaa;
        List<String> list2 = new ArrayList<>();
        for (String string : list) {
            String pieces[] = string.split(username);
            //System.out.println("k" +  pieces.length + pieces[0] + pieces[1]);

            list2.add(pieces[pieces.length - 1]);
        }

        listaaa.clear();
        return list2;
    }

    @Override
    public List<String> listDirectories(String username, String password) throws RemoteException {
        if (!ServerRMI.authenticateUser(username, password)) {
            return null;
        }

        Path path = (Path) FileSystems.getDefault().getPath("", "");
        path = path.toAbsolutePath();
        path = path.resolve(username);

        /*
        File folder = path.toFile();
        File[] listOfFiles = folder.listFiles();

        List<String> list = new ArrayList<>();
        for (File currFile : listOfFiles) {
            if (currFile.isDirectory()) {
                list.add(currFile.getName());
            }
        }
         */
        ServerRMI.walkDir(path.toString());
        List<String> list = listaaa;
        List<String> list2 = new ArrayList<>();
        for (String string : list) {
            String pieces[] = string.split(username);
            //System.out.println("k" +  pieces.length + pieces[0] + pieces[1]);
            String temp = pieces[pieces.length - 1];
            if (!temp.contains(".")) {
                list2.add(temp);
            }
        }

        listaaa.clear();
        return list2;
    }

    @Override
    public boolean copyFile(String filenameWithPath, String destinationDir, String username, String password, boolean backup) throws RemoteException {
        if (!ServerRMI.authenticateUser(username, password)) {
            return false;
        }

        Path path = (Path) FileSystems.getDefault().getPath("", "");
        path = path.toAbsolutePath();

        path = path.resolve(username);
        Path pathSource = path.resolve(filenameWithPath);
        Path pathDestination = path.resolve(destinationDir);
        pathDestination = pathDestination.resolve(pathSource.getFileName());

        if (backup) {
            DAO db = new DAO();
            db.openConnection();

            Usuario u = db.readUsuario(username);

            Mensagem m = new Mensagem();
            m.setNomeDaFuncao("copyFileBackup");
            m.setIp(u.getIpBackup());
            m.setPath(filenameWithPath);
            m.setPath2(destinationDir);
            m.setLogin(username);
            m.setSenha(password);

            db.insertIntoMensagemBackup(m);

            db.close();
        }

        try {
            Files.copy(pathSource, pathDestination);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean moveFile(String filenameWithPath, String destinationDir, String username, String password, boolean backup) throws RemoteException {
        if (!ServerRMI.authenticateUser(username, password)) {
            return false;
        }

        Path path = (Path) FileSystems.getDefault().getPath("", "");
        path = path.toAbsolutePath();

        path = path.resolve(username);
        Path pathSource = path.resolve(filenameWithPath);
        Path pathDestination = path.resolve(destinationDir);
        String pieces[];
        pieces = filenameWithPath.split(Pattern.quote("\\"));
        String add = pieces[pieces.length - 1];
        
        pathDestination = pathDestination.resolve(add);

        if (backup) {
            DAO db = new DAO();
            db.openConnection();

            Usuario u = db.readUsuario(username);

            Mensagem m = new Mensagem();
            m.setNomeDaFuncao("moveFileBackup");
            m.setIp(u.getIpBackup());
            m.setPath(filenameWithPath);
            m.setPath2(destinationDir);
            m.setLogin(username);
            m.setSenha(password);

            db.insertIntoMensagemBackup(m);

            db.close();
        }

        try {
            Files.move(pathSource, pathDestination);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean deleteFile(String filenameWithPath, String username, String password, boolean backup) throws RemoteException {
        if (!ServerRMI.authenticateUser(username, password)) {
            return false;
        }

        Path path = (Path) FileSystems.getDefault().getPath("", "");
        path = path.toAbsolutePath();

        path = path.resolve(username);
        Path pathSource = path.resolve(filenameWithPath);

        if (backup) {
            DAO db = new DAO();
            db.openConnection();

            Usuario u = db.readUsuario(username);

            Mensagem m = new Mensagem();
            m.setNomeDaFuncao("deleteFileBackup");
            m.setIp(u.getIpBackup());
            m.setPath(filenameWithPath);
            m.setLogin(username);
            m.setSenha(password);

            db.insertIntoMensagemBackup(m);

            db.close();
        }

        try {
            Files.deleteIfExists(pathSource);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean createDirectory(String dadDir, String directoryName, String username, String password, boolean backup) throws RemoteException {
        if (!ServerRMI.authenticateUser(username, password)) {
            return false;
        }

        Path path = (Path) FileSystems.getDefault().getPath("", "");
        path = path.toAbsolutePath();
        if (!dadDir.equals("")) {
            path = path.resolve(dadDir);
        }

        path = path.resolve(username);

        path = path.resolve(directoryName);

        File newDir = path.toFile();
        newDir.mkdir();

        if (backup) {
            DAO db = new DAO();
            db.openConnection();
            
            Usuario u = db.readUsuario(username);

            Mensagem m = new Mensagem();
            m.setNomeDaFuncao("createDirBackup");
            m.setIp(u.getIpBackup());
            m.setPath(dadDir);
            m.setPath2(directoryName);
            m.setLogin(username);
            m.setSenha(password);

            db.insertIntoMensagemBackup(m);

            db.close();
        }

        if (newDir.exists()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteDirectory(String dadDir, String directoryName, String username, String password, boolean backup) throws RemoteException {
        if (!ServerRMI.authenticateUser(username, password)) {
            return false;
        }

        Path path = (Path) FileSystems.getDefault().getPath("", "");
        path = path.toAbsolutePath();
        if (!dadDir.equals("")) {
            path = path.resolve(dadDir);
        }

        path = path.resolve(username);
        path = path.resolve(directoryName);

        File newDir = path.toFile();
        newDir.delete();

        if (backup) {
            DAO db = new DAO();
            db.openConnection();

            Usuario u = db.readUsuario(username);

            Mensagem m = new Mensagem();
            m.setNomeDaFuncao("deleteDirBackup");
            m.setIp(u.getIpBackup());
            m.setPath(dadDir);
            m.setPath2(directoryName);
            m.setLogin(username);
            m.setSenha(password);

            db.insertIntoMensagemBackup(m);

            db.close();
        }

        if (newDir.exists()) {
            return false;
        } else {
            return true;
        }
    }
    
    @Override
    public void enviaNovoUsuario (Usuario usuario, int qtdade) throws RemoteException {
        DAO db = new DAO();
        db.openConnection();

        Path path = (Path) FileSystems.getDefault().getPath("", "");
        path = path.toAbsolutePath();
        path = path.resolve(usuario.getLogin());
    
        File newDir = path.toFile();
        newDir.mkdir();
        
        db.insertIntoUsuario(usuario);

        Mensagem m = new Mensagem();
        System.out.println("propagando o cadastro do usuario");
        m.setNomeDaFuncao("enviaNovoUsuario");
        m.setQtdade(qtdade);
        m.setLogin(usuario.getLogin());
        m.setSenha(usuario.getSenha());
        m.setPath(usuario.getIp()); //IP Primario
        m.setPath2(usuario.getIpBackup()); // IP Backup
        db.insertIntoMensagemBackup(m);

        db.close();
    }
    
    @Override
    public List<String> createUser(String username, String password) throws RemoteException {
        DAO db = new DAO();
        db.openConnection();
        
        //logica de atribuir um IP
        List<Host> lista = db.readMeuGrupo();
        RMICommc rmi = null;
        String hostEscolhido = null;
        long menorR = Long.MAX_VALUE;
        long valorAnt = Long.MAX_VALUE;
        String hostAnterior = null;
        boolean first = true;
        for (Host host : lista) {
            if (lista.size() <= 1) {
                hostEscolhido = host.getIp();
                hostAnterior = host.getIp();
                break;
            }
            try {
                rmi = null;
                rmi = (RMICommc) Naming.lookup("rmi://" + host.getIp() + ":" + "/server");

                if (rmi.sizeOfPath() < menorR) {
                    valorAnt = menorR;
                    hostAnterior = hostEscolhido;
                    menorR = rmi.sizeOfPath();
                    hostEscolhido = host.getIp();
                }
            } catch (RemoteException | NotBoundException | MalformedURLException ex) {
                Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("OBJ_CLIENT: me conectei ao " + host.getIp() + "só pra pegar o size");
        }
        rmi = null;
        if (lista.size() > 1 && valorAnt == Long.MAX_VALUE) {
            valorAnt = menorR;
            hostAnterior = hostEscolhido;
        }
        //logica de atribuir um IP

        Usuario user = new Usuario();

        user.setIp(hostEscolhido);
        user.setLogin(username);
        user.setSenha(password);
        user.setIpBackup(hostAnterior);

        if (!db.insertIntoUsuario(user)) {
            db.close();
            return null;
        }
        
        Mensagem m = new Mensagem();
        System.out.println("propagando o cadastro do usuario");
        m.setNomeDaFuncao("enviaNovoUsuario");
        m.setQtdade(db.contaMeuGrupo());
        m.setLogin(user.getLogin());
        m.setSenha(user.getSenha());
        m.setPath(user.getIp()); //IP Primario
        m.setPath2(user.getIpBackup()); // IP Backup
        db.insertIntoMensagemBackup(m);

        db.close();

        List<String> list = new ArrayList<>();
        list.add(hostEscolhido);
        list.add(hostAnterior);
        return list;
    }

    public static void main(String args[]) {
        boolean isPTE;
        String ttt = JOptionPane.showInputDialog("Digite sim ou nao");
        if(ttt.equals("sim")){
            isPTE = true;
        } else {
            isPTE = false;
        }
        
        //String IpDoPTE = "192.168.0.15";
        String IpDoPTE = JOptionPane.showInputDialog("Digite o IP do PTE");
        //-------------------------------------------------
        InetAddress IP = null;
        try {
            IP = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
        }
        String myIp = IP.getHostAddress();
        //-------------------------------------------------
        DAO db;
        db = new DAO();

        db.openConnection();
        db.deleteAllFromMeuGrupo();
        db.deleteFromMensagem();
        db.deletaParar();
        db.inicializaBreak();

        db.close();
        //-------------------------------------------------
        try {
            Policy.setPolicy(new MyPolicy());
            System.setSecurityManager(new RMISecurityManager());
            System.setProperty("java.rmi.server.hostname", myIp);
            LocateRegistry.createRegistry(Const.PORTA_PADRAO);

            try {
                Naming.rebind("rmi://:/server", new ServerRMI());
            } catch (MalformedURLException ex) {
                Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("OBJ_SERVER: Ligado no registro da porta " + Const.PORTA_PADRAO);
        } catch (RemoteException ex) {
            Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
        }
        //-------------------------------------------------
        if (isPTE) {
            db = new DAO();
            db.openConnection();

            Host host = new Host();

            host.setIp(myIp);

            if (db.insertIntoMeuGrupoSemId(host)) {
                System.out.println("OBJ_SERVER: first of his group inserted.");
                host = db.retriveIdAfterInsert();
                myFutureId = host.getId();
            } else {
                System.out.println("OBJ_SERVER: first of his group not inserted.");
            }
            db.close();
        } else {
            RMIClientCommc objetoCliente = new RMIClientCommc(IpDoPTE, true, 0);
            Thread t = new Thread(objetoCliente);
            t.start();
        }
    }

    @Override
    public boolean areYouAlive(String text) throws RemoteException {
        System.out.println("OBJ_SERVER: Oh yes, I'm.");
        return true;
    }

    @Override
    public List<Host> joinSystem(String ipIngressante) throws RemoteException {
        DAO dao = new DAO();
        dao.openConnection();

        Host host = new Host();
        host.setIp(ipIngressante);
        dao.insertIntoMeuGrupoSemId(host);

        dao.setParar1();

        dao.close();

        try {
            sleep(Const.TIME_TO_SLEEP + 1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
        }

        dao = new DAO();
        dao.openConnection();

        dao.setParar0();

        RMIClientCommc objetoCliente = new RMIClientCommc(ipIngressante, false, myFutureId);
        Thread t = new Thread(objetoCliente);
        t.start();

        List<Host> lista = dao.readMeuGrupo();
        Mensagem m = new Mensagem();
        System.out.println("criando a msg de enviaIPs no joinSystem");
        m.setNomeDaFuncao("enviaIPs");
        m.setQtdade(lista.size());
        dao.insertIntoMensagem(m);

        dao.close();
        
        int listaSize = lista.size();
        ThreadAux obj = new ThreadAux(listaSize);
        Thread th = new Thread(obj);
        th.start();
        /*
        try {
            sleep(Const.TIME_TO_SLEEP + 500);
        } catch (InterruptedException ex) {
            Logger.getLogger(ServerRMI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        dao = new DAO();
        dao.openConnection();
        
        m = new Mensagem();
        System.out.println("criando a msg de enviarAllUsersToNewServer no joinSystem");
        m.setNomeDaFuncao("enviarAllUsersToNewServer");
        m.setQtdade(lista.size());
        dao.insertIntoMensagem(m);

        dao.close();
        */
        return lista;
    }
    
    @Override
    public void inserirUsuariosPqEuSouNovo(List<Usuario> usersList) throws RemoteException {
        DAO db = new DAO();
        db.openConnection();
        
        for (Usuario usuario : usersList) {
            db.insertIntoUsuario(usuario);
        }
        
        db.close();
    }

    @Override
    public void deleteServerFromMeuGrupo(String ipToDelete, int qtdade) throws RemoteException {
        DAO db = new DAO();
        db.openConnection();

        db.deleteFromMeuGrupoByIP(ipToDelete);

        Mensagem m = new Mensagem();
        System.out.println("criando a msg de deletaIPX no deleteServerFromMeuGrupo" + ipToDelete);
        m.setNomeDaFuncao("deletaIPX");
        m.setIp(ipToDelete);
        m.setQtdade(qtdade);
        db.insertIntoMensagemWithIP(m);

        db.close();
    }

    @Override
    public void enviaIPs(List<Host> listaDeHosts, int qtdade) throws RemoteException {
        DAO db = new DAO();
        db.openConnection();

        for (Host host : listaDeHosts) {
            db.insertIntoMeuGrupo(host);
        }

        Mensagem m = new Mensagem();
        System.out.println("criando a msg de enviaIPs na func enviaIPs");
        m.setNomeDaFuncao("enviaIPs");
        m.setQtdade(qtdade);
        db.insertIntoMensagem(m);

        db.close();
    }

    @Override
    public void enviaBackup(Mensagem m) throws RemoteException {
        DAO db = new DAO();
        db.openConnection();

        System.out.println("criando a msg de enviaIPs na func " + m.getNomeDaFuncao());
        m.setQtdade(m.getQtdade() - 1);

        //db.insertIntoMensagemBackup(m);
        db.close();
    }

    
    @Override
    public List<String> allActiveServers() throws RemoteException {
        DAO db = new DAO();
        db.openConnection();

        List<String> list = new ArrayList<>();
        List<Host> hList = db.readMeuGrupo();
        for (Host host : hList) {
            list.add(host.getIp());
        }

        db.close();

        return list;
    }



    public static class MyPolicy extends Policy {

        @Override
        public PermissionCollection getPermissions(CodeSource codesource) {
            Permissions p = new Permissions();
            p.add(new java.security.AllPermission());
            return p;
        }
    }

}
