package clientrmi;

import global.Const;
import dao.DAO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import model.Diretorio;
import model.Host;
import model.Mensagem;
import model.Usuario;
import rmi.RMI;
import rmi.RMICommc;

public class RMIClientCommc implements Runnable {

    private static RMICommc rmi;
    private static RMI rmiTemp;
    private static RMI rmiTemp2;
    private String IPMyServer;
    private final String myIp;
    private int myId;
    private boolean ehPraEntrarEmGrupo;

    public RMIClientCommc(String ip, boolean ehPra, int futureId) {
        if (!ehPra) {
            myId = futureId;
        }

        ehPraEntrarEmGrupo = ehPra;
        this.IPMyServer = ip;
        InetAddress IP2 = null;
        try {
            IP2 = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
        }
        myIp = IP2.getHostAddress();
    }

    public void connectToServerPTE() {
        try {
            rmi = (RMICommc) Naming.lookup("rmi://" + this.IPMyServer + ":" + "/server");
        } catch (RemoteException | NotBoundException | MalformedURLException ex) {
            Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("OBJ_CLIENT: inicial: me conectei ao " + this.IPMyServer);
    }

    public void connectToServer(String ip) {
        try {
            rmi = (RMICommc) Naming.lookup("rmi://" + ip + ":" + "/server");
        } catch (RemoteException | NotBoundException | MalformedURLException ex) {
            Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("OBJ_CLIENT: me conectei ao " + ip);
    }

    public void isServerAlive() throws RemoteException {
        System.out.println("OBJ_CLIENT: are you alive " + IPMyServer + "?");
        boolean r = rmi.areYouAlive("OBJ_CLIENT: are you alive " + IPMyServer + "?");
    }

    public void serverCaiu() {
        DAO db = new DAO();
        db.openConnection();

        List<Host> lista = db.readMeuGrupo();
        Host newHostToConnect = null;
        Host hostToDelete = null;

        for (Host host : lista) {// servers: 0 -> 3 -> 2 -> 1 -> 0
            //System.out.println("huuum" + host.getIp() + host.getId());
            if (host.getId() == myId) {
                //System.out.println("entrou, because: " + myId);
                int index = lista.indexOf(host);//0
                if (lista.size() == 2) {
                    if (index == 0) {
                        hostToDelete = lista.get(1);
                        newHostToConnect = lista.get(0);
                    } else {
                        hostToDelete = lista.get(0);
                        newHostToConnect = lista.get(1);
                    }
                    break;
                }
                int index2 = index - 1;//-1
                if (index2 >= 0) {
                    hostToDelete = lista.get(index2);
                } else {
                    hostToDelete = lista.get(lista.size() - 1);
                }
                index--;
                if (index == -1) {
                    index = lista.size() - 1;
                }
                //System.out.println("index is: " + index);
                //System.out.println("lista size is: " + lista.size());
                if ((index + 1) < lista.size()) {
                    if ((index + 2) < lista.size()) {
                        newHostToConnect = lista.get(index + 2);
                    } else {
                        newHostToConnect = lista.get(0);
                    }
                } else {
                    newHostToConnect = lista.get(1);
                }
            }
        }
        
        List<Usuario> listUsers = db.readAllUsuarios();
        List<Usuario> listUsersAfetados = new ArrayList<>();
        for (Usuario el : listUsers) {
            if(el.getIp().equals(hostToDelete.getIp()) || el.getIpBackup().equals(hostToDelete.getIp())){
                listUsersAfetados.add(el);
            }
        }
        String ipDoQueCaiu = hostToDelete.getIp();
        List<Host> listaNova = new ArrayList<>();
        for (Host host : lista) {
            if(!host.getIp().equals(ipDoQueCaiu))
                listaNova.add(host);
        }

        db.deleteFromMeuGrupoByIP(hostToDelete.getIp());

        Mensagem m = new Mensagem();
        System.out.println("criando a msg de deletaIPX no serverCaiu" + hostToDelete.getIp());
        m.setNomeDaFuncao("deletaIPX");
        m.setIp(hostToDelete.getIp());
        m.setQtdade(lista.size());
        db.insertIntoMensagemWithIP(m);

        db.close();

        this.IPMyServer = newHostToConnect.getIp();
        this.connectToServer(newHostToConnect.getIp());
        
//        try {
//            sleep(Const.TIME_TO_SLEEP + 500);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        String ipToConnect;
        List<Usuario> listUsersAfetadosNova = new ArrayList<>();
        for (Usuario el : listUsersAfetados) {
            if(el.getIp().equals(ipDoQueCaiu)){
                ipToConnect = el.getIpBackup();
            } else {
                ipToConnect = el.getIp();
            }
            
            try {
                rmiTemp = (RMI) Naming.lookup("rmi://" + ipToConnect + ":" + "/server");
            } catch (RemoteException | NotBoundException | MalformedURLException ex) {
                Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if(myIp.equals(el.getIp()) || myIp.equals(el.getIpBackup())){
                //---------------------------------------------------------------------------------------
                try {
                    rmiTemp2 = (RMI) Naming.lookup("rmi://" + IPMyServer + ":" + "/server");
                } catch (RemoteException | NotBoundException | MalformedURLException ex) {
                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    List<String> dirList = rmiTemp.listDirectories(el.getLogin(), el.getSenha());
                    List<Diretorio> dirList2 = new ArrayList<>();
                    Diretorio dir;
                    for (String string : dirList) {
                        dir = new Diretorio();
                        dir.setDir(string.substring(1, string.length() - 1));
                        String pieces[];
                        pieces = string.split(Pattern.quote("\\"));
                        dir.setQtdadeSep(pieces.length - 1);
                        dirList2.add(dir);
                    }
                    Collections.sort(dirList2);
                    
                    for (Diretorio diretorio : dirList2) {
                        rmiTemp2.createDirectory("", diretorio.getDir(), el.getLogin(), el.getSenha(), false);
                    }
                } catch (RemoteException ex) {
                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    List<String> filesList = rmiTemp.listFiles(el.getLogin(), el.getSenha());
                    byte[] receivedData;

                    for (String filePath : filesList) {
                        filePath = filePath.substring(1, filePath.length() - 1);
                        receivedData = rmiTemp.downloadFile(el.getLogin(), filePath, el.getSenha());

                        rmiTemp2.uploadFile(el.getLogin(), filePath, receivedData, el.getSenha(), false);
                    }
                } catch (RemoteException ex) {
                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                }
                //---------------------------------------------------------------------------------------
            } else {
                try {
                    List<String> dirList = rmiTemp.listDirectories(el.getLogin(), el.getSenha());
                    List<Diretorio> dirList2 = new ArrayList<>();
                    Diretorio dir;
                    for (String string : dirList) {
                        dir = new Diretorio();
                        dir.setDir(string.substring(1, string.length() - 1));
                        String pieces[];
                        pieces = string.split(Pattern.quote("\\"));
                        dir.setQtdadeSep(pieces.length - 1);
                        dirList2.add(dir);
                    }
                    Collections.sort(dirList2);
                    Path path = (Path) FileSystems.getDefault().getPath("", "");
                    path = path.toAbsolutePath();
                    path = path.resolve(el.getLogin());
                    Path path2;
                    File newDir;
                    for (Diretorio diretorio : dirList2) {
                        path2 = path.resolve(diretorio.getDir());
                        newDir = path2.toFile();
                        newDir.mkdir();
                    }
                } catch (RemoteException ex) {
                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    List<String> filesList = rmiTemp.listFiles(el.getLogin(), el.getSenha());
                    byte[] receivedData;
                    Path path = (Path) FileSystems.getDefault().getPath("", "");
                    path = path.toAbsolutePath();
                    path = path.resolve(el.getLogin());
                    for (String filePath : filesList) {
                        filePath = filePath.substring(1, filePath.length() - 1);
                        receivedData = rmiTemp.downloadFile(el.getLogin(), filePath, el.getSenha());

                        Path path2 = path.resolve(filePath);
                        FileOutputStream stream = null;
                        try {
                            stream = new FileOutputStream(path2.toFile());
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        try {
                            stream.write(receivedData);
                        } catch (IOException ex) {
                            Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
                        } finally {
                            try {
                                stream.close();
                            } catch (IOException ex) {
                                Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                } catch (RemoteException ex) {
                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            db = new DAO();
            db.openConnection();
            
            if(el.getIp().equals(ipDoQueCaiu)){
                db.setNewIpForUser(myIp, el.getLogin());
                el.setIp(myIp);
            } else {
                db.setNewIpBackupForUser(myIp, el.getLogin());
                el.setIpBackup(myIp);
            }
            
            listUsersAfetadosNova.add(el);
        }
        for (Host h : listaNova) {
            try {
                rmiTemp = (RMI) Naming.lookup("rmi://" + h.getIp() + ":" + "/server");
            } catch (RemoteException | NotBoundException | MalformedURLException ex) {
                Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (Usuario u : listUsersAfetadosNova) {
                try {
                    rmiTemp.updateUser(u);
                } catch (RemoteException ex) {
                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void enterInGroup() {
        List<Host> lista = null;
        try {
            lista = rmi.joinSystem(myIp);
        } catch (RemoteException ex) {
            Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
        }

        DAO db = new DAO();
        db.openConnection();

        for (Host host : lista) {
            db.insertIntoMeuGrupo(host);
            host = db.retriveIdAfterInsert();
        }

        this.myId = db.selectMaxMeuGrupo();
        db.close();

        int maiorId = 0;
        Host meuServer = null;
        for (Host host : lista) {
            if (host.getId() > maiorId && host.getId() != myId) {
                maiorId = host.getId();
                meuServer = host;
                IPMyServer = host.getIp();
            }
        }

        this.connectToServer(meuServer.getIp());
    }

    public void enviaIPs(List<Host> listaDeHosts, int qtdade) {
        try {
            rmi.enviaIPs(listaDeHosts, qtdade);
        } catch (RemoteException ex) {
            Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void enviaNovoUsuario(Usuario usuario, int qtdade) {
        try {
            rmi.enviaNovoUsuario(usuario, qtdade);
        } catch (RemoteException ex) {
            Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void enviarAllUsersToNewServer(){
        DAO db = new DAO();
        db.openConnection();
        
        List<Usuario> usersList = db.readAllUsuarios();
        
        try {
            rmi.inserirUsuariosPqEuSouNovo(usersList);
        } catch (RemoteException ex) {
            Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
    }

    public void deletaIP(String ipToDelete, int qtdade) {
        System.out.println("ddsss" + ipToDelete);
        try {
            rmi.deleteServerFromMeuGrupo(ipToDelete, qtdade);
        } catch (RemoteException ex) {
            Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        DAO db;
        connectToServerPTE();

        if (ehPraEntrarEmGrupo) {
            enterInGroup();
        }

        while (true) {
            db = new DAO();
            db.openConnection();

            if (db.ehPraParar()) {
                db.setParar0();
                db.close();
                break;
            }
            db.close();

            try {
                Thread.sleep(Const.TIME_TO_SLEEP);
            } catch (InterruptedException ex) {
                Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                db = new DAO();
                db.openConnection();

                Mensagem m = db.readMensagens();

                db.close();

                if (m != null) {
                    if (m.getNomeDaFuncao() != null) {
                        switch (m.getNomeDaFuncao()) {
                            case "enviaIPs": {
                                System.out.println("passando pelo switch e case enviaIPs");
                                db = new DAO();
                                db.openConnection();
                                db.deleteFromMensagem();

                                if (m.getQtdade() == 0) {
                                    break;
                                }

                                List<Host> lista = db.readMeuGrupo();
                                db.close();
                                enviaIPs(lista, m.getQtdade() - 1);
                                break;
                            }
                            case "enviaNovoUsuario": {
                                System.out.println("passando pelo switch e case enviaNovoUsuario");
                                db = new DAO();
                                db.openConnection();
                                db.deleteFromMensagem();
                                db.close();

                                if (m.getQtdade() == 0) {
                                    break;
                                }

                                Usuario usuario = new Usuario();
                                usuario.setLogin(m.getLogin());
                                usuario.setSenha(m.getSenha());
                                usuario.setIp(m.getPath());
                                usuario.setIpBackup(m.getPath2());
                                System.out.println("eieiei "+usuario.getLogin());
                                enviaNovoUsuario(usuario, m.getQtdade() - 1);
                                break;
                            }
                            case "deletaIPX": {
                                System.out.println("passando pelo switch e case deletaIPX" + m.getIp());
                                db = new DAO();
                                db.openConnection();
                                db.deleteFromMensagem();

                                if (m.getQtdade() == 0) {
                                    break;
                                }

                                db.close();

                                deletaIP(m.getIp(), m.getQtdade() - 1);
                                break;
                            }
                            case "uploadBackup": {
                                System.out.println("passando pelo switch case uploadBackup");
                                db = new DAO();
                                db.openConnection();
                                db.deleteFromMensagem();
                                db.close();

                                RMI rmi2 = null;

                                try {
                                    rmi2 = (RMI) Naming.lookup("rmi://" + m.getIp() + ":" + "/server");
                                } catch (NotBoundException | MalformedURLException | RemoteException ex) {
                                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                Path path = (Path) FileSystems.getDefault().getPath("", "");
                                path = path.toAbsolutePath();
                                path = path.resolve(m.getLogin());
                                path = path.resolve(m.getPath());

                                byte[] data = null;

                                try {
                                    data = Files.readAllBytes(path);
                                } catch (IOException ex) {
                                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                rmi2.uploadFile(m.getLogin(), m.getPath(), data, m.getSenha(), false);

                                break;
                            }
                            case "copyFileBackup": {
                                System.out.println("passando pelo switch case copyFileBackup");
                                db = new DAO();
                                db.openConnection();
                                db.deleteFromMensagem();
                                db.close();

                                RMI rmi2 = null;

                                try {
                                    rmi2 = (RMI) Naming.lookup("rmi://" + m.getIp() + ":" + "/server");
                                } catch (NotBoundException | MalformedURLException | RemoteException ex) {
                                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                rmi2.copyFile(m.getPath(), m.getPath2(), m.getLogin(), m.getSenha(), false);

                                break;
                            }
                            case "moveFileBackup": {
                                System.out.println("passando pelo switch case moveFileBackup");
                                db = new DAO();
                                db.openConnection();
                                db.deleteFromMensagem();
                                db.close();

                                RMI rmi2 = null;

                                try {
                                    rmi2 = (RMI) Naming.lookup("rmi://" + m.getIp() + ":" + "/server");
                                } catch (NotBoundException | MalformedURLException | RemoteException ex) {
                                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                rmi2.moveFile(m.getPath(), m.getPath2(), m.getLogin(), m.getSenha(), false);

                                break;
                            }
                            case "deleteFileBackup": {
                                System.out.println("passando pelo switch case deleteFileBackup");
                                db = new DAO();
                                db.openConnection();
                                db.deleteFromMensagem();
                                db.close();

                                RMI rmi2 = null;

                                try {
                                    rmi2 = (RMI) Naming.lookup("rmi://" + m.getIp() + ":" + "/server");
                                } catch (NotBoundException | MalformedURLException | RemoteException ex) {
                                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                rmi2.deleteFile(m.getPath(), m.getLogin(), m.getSenha(), false);

                                break;
                            }
                            case "createDirBackup": {
                                System.out.println("passando pelo switch case createDirBackup");
                                db = new DAO();
                                db.openConnection();
                                db.deleteFromMensagem();
                                db.close();

                                RMI rmi2 = null;

                                try {
                                    rmi2 = (RMI) Naming.lookup("rmi://" + m.getIp() + ":" + "/server");
                                } catch (NotBoundException | MalformedURLException | RemoteException ex) {
                                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                rmi2.createDirectory(m.getPath(), m.getPath2(), m.getLogin(), m.getSenha(), false);

                                break;
                            }
                            case "deleteDirBackup": {
                                System.out.println("passando pelo switch case deleteDirBackup");
                                db = new DAO();
                                db.openConnection();
                                db.deleteFromMensagem();
                                db.close();

                                RMI rmi2 = null;

                                try {
                                    rmi2 = (RMI) Naming.lookup("rmi://" + m.getIp() + ":" + "/server");
                                } catch (NotBoundException | MalformedURLException | RemoteException ex) {
                                    Logger.getLogger(RMIClientCommc.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                rmi2.deleteDirectory(m.getPath(), m.getPath2(), m.getLogin(), m.getSenha(), false);

                                break;
                            }
                            case "enviarAllUsersToNewServer": {
                                db = new DAO();
                                db.openConnection();
                                db.deleteFromMensagem();
                                db.close();
                                
                                enviarAllUsersToNewServer();
                                
                                break;
                            }
                            default: {
                                break;
                            }
                        }
                    } else {
                        isServerAlive();
                    }
                }
                System.out.println("OBJ_CLIENT: he is.");
            } catch (RemoteException ex) {
                System.out.println("OBJ_CLIENT: server caiu.");
                serverCaiu();
                //break;
            }
        }
    }

}
