package clientrmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import rmi.RMICommc;

public class AppSupervisorClient {
    
    public static RMICommc rmi;
    public static RMICommc rmi2;
    public String ipToConnect;
    
    public AppSupervisorClient(String ip) {
        this.ipToConnect = ip;
    }
    
    public void connectToServer(){
        try {
            rmi = (RMICommc) Naming.lookup("rmi://" + this.ipToConnect + ":" + "/server");
        } catch (NotBoundException | MalformedURLException | RemoteException ex) {
            Logger.getLogger(AppSupervisorClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<String> allFilesOfServerX(String givenIP){
        try {
            rmi2 = (RMICommc) Naming.lookup("rmi://" + givenIP + ":" + "/server");
        } catch (NotBoundException | MalformedURLException | RemoteException ex) {
            Logger.getLogger(AppSupervisorClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<String> resultados = null;
        try {
            resultados = rmi2.allFilesOfTheServer();
        } catch (RemoteException ex) {
            Logger.getLogger(AppSupervisorClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultados;
    }
    
    public List<String> requestAllActiveServers() {
        List<String> resultados = null;
        try {
            resultados = rmi.allActiveServers();
        } catch (RemoteException ex) {
            Logger.getLogger(AppSupervisorClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultados;
    }
    
}
