package clientrmi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Usuario;
import rmi.RMI;

public class RMIClient {

    public static RMI rmi;
    public final String IPPTE;
    public String IPPrimario = "";
    public String IPBackup = "";

    public RMIClient(String ip) {
        this.IPPTE = ip;
    }

    public void sayHelloToServer() throws RemoteException {
        String text = null;
        text = rmi.helloRMI("hi server");

        System.out.println(text);
    }

    public boolean downloadFileFromServer(String username, String pathname, String password, String pathToDownload) {
        try {
            byte[] receivedData = rmi.downloadFile(username, pathname, password);
            if (receivedData == null) {
                return false;
            }

            FileOutputStream stream = null;
            try {
                stream = new FileOutputStream(pathToDownload);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
            try {
                stream.write(receivedData);
            } catch (IOException ex) {
                Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            } finally {
                try {
                    stream.close();
                } catch (IOException ex) {
                    Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }
            }
        } catch (RemoteException ex) {
            try {
                connectServer2();
            } catch (RemoteException e) {
                return false;
            }
        }
        return true;
    }

    public boolean uploadFileToServer(String username, String pathname, String password, String pathDestNuvem) {
        //Path path = (Path) FileSystems.getDefault().getPath("", "");
        //path = path.resolve(pathname);

        byte[] data = null;
        File f = new File(pathname);
        Path p = f.toPath();
        try {
            data = Files.readAllBytes(p);
            return rmi.uploadFile(username, pathDestNuvem, data, password, true);
            
        } catch (IOException ex) {
            try {
                connectServer2();
            } catch (RemoteException e) {
                return false;
            }
            return false;
        }
    }

    public String listFilesAndDirectoriesFromServer(String username, String password) {
        String retorno = "";
        try {
            List<String> list = rmi.listFiles(username, password);
            if (list == null) {
                return null;
            }

            System.out.println("Arquivos do " + username + ": ");
            retorno = retorno.concat("Arquivos do " + username + ": " + "\n");
            for (String el : list) {
                System.out.println(el);
                retorno = retorno.concat(el);
            }

            List<String> list2 = rmi.listDirectories(username, password);
            if (list2 == null) {
                return null;
            }

            System.out.println("Diretorios do " + username + ": ");
            retorno = retorno.concat("Diretorios do " + username + ": " + "\n");
            for (String el : list2) {
                System.out.println(el);
                retorno = retorno.concat(el);
            }
        } catch (RemoteException ex) {
            try {
                connectServer2();
            } catch (RemoteException e) {
                return null;
            }
            return null;
        }

        return retorno;
    }
    
    public String listFilesFromServer(String username, String password) {
        String retorno = "";
        try {
            List<String> list = rmi.listFiles(username, password);
            if (list == null) {
                return null;
            }

            System.out.println("Arquivos do " + username + ": ");
            retorno = retorno.concat("Arquivos do " + username + ": " + "\n");
            for (String el : list) {
                System.out.println(el);
                retorno = retorno.concat(el);
            }
        } catch (RemoteException ex) {
            try {
                connectServer2();
            } catch (RemoteException e) {
                return null;
            }
            return null;
        }

        return retorno;
    }
    
    public String listDirectoriesFromServer(String username, String password) {
        String retorno = "";
        try {
            List<String> list2 = rmi.listDirectories(username, password);
            if (list2 == null) {
                return null;
            }

            System.out.println("Diretorios do " + username + ": ");
            retorno = retorno.concat("Diretorios do " + username + ": " + "\n");
            for (String el : list2) {
                System.out.println(el);
                retorno = retorno.concat(el);
            }
        } catch (RemoteException ex) {
            try {
                connectServer2();
            } catch (RemoteException e) {
                return null;
            }
            return null;
        }

        return retorno;
    }

    public String criaUserNoServer(String username, String password) {
        try {
            List<String> ret = rmi.createUser(username, password);
            this.IPPrimario = ret.get(0);
            this.IPBackup = ret.get(1);
            connectServer2();
            return "sucesso";
        } catch (RemoteException ex) {
            try {
                connectServer2();
            } catch (RemoteException e) {
                return null;
            }
            return null;
        }
    }

    public boolean criaDiretorioNoServer(String dadDir, String directoryName, String username, String password) {
        try {
            return rmi.createDirectory(dadDir, directoryName, username, password, true);
        } catch (RemoteException ex) {
            try {
                connectServer2();
            } catch (RemoteException e) {
                return false;
            }
            return false;
        }
    }

    public boolean deletaDiretorioNoServer(String dadDir, String directoryName, String username, String password) {
        try {
            return rmi.deleteDirectory(dadDir, directoryName, username, password, true);
        } catch (RemoteException ex) {
            try {
                connectServer2();
            } catch (RemoteException e) {
                return false;
            }
            return false;
        }
    }

    public boolean copiaArquivoNoServer(String filenameWithPath, String destinationDir, String username, String password) {
        try {
            return rmi.copyFile(filenameWithPath, destinationDir, username, password, true);
        } catch (RemoteException ex) {
            try {
                connectServer2();
            } catch (RemoteException e) {
                return false;
            }
            return false;
        }
    }

    public boolean moveArquivoNoServer(String filenameWithPath, String destinationDir, String username, String password) {
        try {
            return rmi.moveFile(filenameWithPath, destinationDir, username, password, true);
        } catch (RemoteException ex) {
            try {
                connectServer2();
            } catch (RemoteException e) {
                return false;
            }
            return false;
        }
    }

    public boolean deletaArquivoNoServer(String filenameWithPath, String username, String password) {
        try {
            return rmi.deleteFile(filenameWithPath, username, password, true);
        } catch (RemoteException ex) {
            try {
                connectServer2();
            } catch (RemoteException e) {
                return false;
            }
            return false;
        }
    }

    public void connectServer2() throws RemoteException {
        try {
            rmi = (RMI) Naming.lookup("rmi://" + this.IPPrimario + ":" + "/server");
        } catch (NotBoundException | MalformedURLException | RemoteException ex) {
            //Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
            try {
                rmi = (RMI) Naming.lookup("rmi://" + this.IPBackup + ":" + "/server");
            } catch (NotBoundException | MalformedURLException | RemoteException ex2) {
                //Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex2);
                try {
                    rmi = (RMI) Naming.lookup("rmi://" + this.IPPTE + ":" + "/server");
                } catch (NotBoundException | MalformedURLException | RemoteException ex1) {
                    Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        }
    }
    
    public void connectServer() {
        try {
            rmi = (RMI) Naming.lookup("rmi://" + this.IPPTE + ":" + "/server");
        } catch (NotBoundException | MalformedURLException | RemoteException ex) {
            Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void disconnectServer() {
        rmi = null;
    }
    
    public boolean login(String login, String senha) {
        try {
            Usuario user = rmi.authenticateUserAndReturnInfos(login, senha);
            if(user != null){
                this.IPPrimario = user.getIp();
                this.IPBackup = user.getIpBackup();
                connectServer2();
            }
            return true;
        } catch (RemoteException ex) {
            Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static void main(String args[]) {
        RMIClient client = new RMIClient(JOptionPane.showInputDialog("Digite o IP do PTE"));
        //System.out.println("aff");
        client.connectServer();
        
        //client.listFilesAndDirectoriesFromServer("lucas", "senha");

        try {
            client.sayHelloToServer();
            
            //
            
            client.criaUserNoServer("brabum", "brabum");
            //client.uploadFileToServer("lucas", "C:\\Users\\Leandro\\Documents\\UEL\\Lab Prog\\Lab_Aula4.pdf", "senha", "dasdas.pdf");
            //client.downloadFileFromServer("lucas", "arqpteste1.txt");
            //client.criaUserNoServer("username1", "secretPassword");
            //client.criaDiretorioNoServer("", "", "brabum", "brabum");
            //client.deletaDiretorioNoServer("", "outroFilho\\neto", "lucas", "senha");
            //client.copiaArquivoNoServer("dir1\\outroAinda.txt", "outroFilho", "lucas", "senha");
            //client.moveArquivoNoServer("outroFilho\\outroAinda.txt", "outroFilho\\neto", "lucas", "senha");
            //client.deletaArquivoNoServer("outroFilho\\neto", "lucas", "senha");
        } catch (RemoteException ex) {
            Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
