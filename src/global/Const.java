package global;

import java.util.logging.Level;

public class Const {
    
    public final static String PORTA_PADRAO_S = "1099";
    public final static int PORTA_PADRAO = 1099;
    public final static int TIME_TO_SLEEP = 2222;
    public final static boolean PRINT_EXCEPTION = false;
    //public final static Level LOG_LEVEL = java.util.logging.Level.WARNING;
    public final static Level LOG_LEVEL = java.util.logging.Level.OFF;
    public final static String BD_FILE_NAME = "testeBDcomER.db";
}
